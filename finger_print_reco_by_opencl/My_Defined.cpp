﻿#include <QCoreApplication>
//used of c header
#include <stdio.h>
#include <math.h>
#define _USE_MATH_DEFINES // for C++
#include <cmath>
//used of  /c++ header
#include <sstream>
#include <iostream>
#include <vector>
#include <array>
//used of opencv header
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d/features2d.hpp"
//use of opencl header
#ifdef __APPLE__
#include <OpenCL/opencl.h> //if in apple platform
#else
#include <CL/cl.h> //if in windows platform
#endif

#include "My_Defined.hpp"



/*polluted input array with random noise(gauss noise)*/
void  My_defined_space::polluting::polluting_double
    (const double polluted_average,const double polluted_std_dev, const cv::Mat& input_array, cv::Mat& output_array) const
{// declear default argument in implemnted-header.cpp

        cv::Mat input_array_cv64fc1(input_array.size(), CV_64FC1);
        //input_array.convertTo(input_array_cv64fc1, CV_64FC1);
        // if only use it , output image will be "all white", because 0 < uchar_image < 255 , 0 < float or doble < 1
        // and convertTo just linear transform => y(i,j) = a*(x(i,j)+b) => (float)y(i,j) = (float)(x(i,j)+b)
        //(float)y(i,j) = (float)(x(i,j)+b) can't limit y(i,j) between 0 and 1
        //so need to use input_array.convertTo(input_array_cv64fc1, CV_64FC1 , 1.0/255.0);
        //or use normalize(input_array, input_array_cv64fc1, 0.0, 1.0, CV_MINMAX, CV_64F);
        normalize(input_array, input_array_cv64fc1, 0.0, 1.0, CV_MINMAX, CV_64F);//input_array.convertTo(input_array_cv64fc1, CV_64FC1, 1.0 / 255.0);
        cv::Mat rand_array(input_array.size(), CV_64FC1);
        input_array.convertTo(output_array, CV_64FC1);

		//pollute
		cv::randn(rand_array, polluted_average, polluted_std_dev);
		//add noise from input to output
		for (int i = 0; i < input_array.rows; i++) {
			for (int j = 0; j < input_array.cols; j++) {
				output_array.at<double>(i, j) =
					cv::saturate_cast<double>(input_array_cv64fc1.at<double>(i, j) + rand_array.at<double>(i, j));
			}
		} //do add , type of every element need to match
}



//*polluted input array with random noise(gauss noise)
//==output type is uchar==//
void My_defined_space::polluting::
    polluting_uchar
    (const double polluted_average, const double polluted_std_dev, const cv::Mat & input_array, cv::Mat & output_array) const{
    cv::Mat input_array_cv8uc1 , mGaussian_noise;
    cv::Mat mGaussian_noise_buf = cv::Mat(input_array.size(), CV_8U);
    //cv::Mat mGaussian_noise = cv::Mat(input_array.size(), CV_8U);
    //normalize(input_array, input_array_cv8uc1, 0, 255, CV_MINMAX, CV_8U);
    cv::randn(mGaussian_noise_buf, polluted_average, polluted_std_dev);
    normalize(mGaussian_noise_buf, mGaussian_noise, 0, 255, CV_MINMAX, CV_8U);
    cv::add(input_array, mGaussian_noise_buf, output_array);
    //output_array = input_array + mGaussian_noise_buf;
}




void My_defined_space::thinning::thinningIteration_uhar(const cv::Mat& img, int iter) const
{
    cv::Mat del_marker = cv::Mat::ones(img.size(), CV_8U);
    int x, y;

    for (y = 1; y < img.rows - 1; ++y) {

        for (x = 1; x < img.cols - 1; ++x) {

            int v9, v2, v3;
            int v8, v1, v4;
            int v7, v6, v5;

            v1 = img.data[y   * img.step + x   * img.elemSize()];
            v2 = img.data[(y - 1) * img.step + x   * img.elemSize()];
            v3 = img.data[(y - 1) * img.step + (x + 1) * img.elemSize()];
            v4 = img.data[y   * img.step + (x + 1) * img.elemSize()];
            v5 = img.data[(y + 1) * img.step + (x + 1) * img.elemSize()];
            v6 = img.data[(y + 1) * img.step + x   * img.elemSize()];
            v7 = img.data[(y + 1) * img.step + (x - 1) * img.elemSize()];
            v8 = img.data[y   * img.step + (x - 1) * img.elemSize()];
            v9 = img.data[(y - 1) * img.step + (x - 1) * img.elemSize()];

            int S = (v2 == 0 && v3 == 1) + (v3 == 0 && v4 == 1) +
                (v4 == 0 && v5 == 1) + (v5 == 0 && v6 == 1) +
                (v6 == 0 && v7 == 1) + (v7 == 0 && v8 == 1) +
                (v8 == 0 && v9 == 1) + (v9 == 0 && v2 == 1);

            int N = v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9;

            int m1 = 0, m2 = 0;

            if (iter == 0) m1 = (v2 * v4 * v6);
            if (iter == 1) m1 = (v2 * v4 * v8);

            if (iter == 0) m2 = (v4 * v6 * v8);
            if (iter == 1) m2 = (v2 * v6 * v8);

            if (S == 1 && (N >= 2 && N <= 6) && m1 == 0 && m2 == 0)
                del_marker.data[y * del_marker.step + x * del_marker.elemSize()] = 0;
        }
    }

    img &= del_marker;
}

void  My_defined_space::thinning::thinning_uhar(const cv::Mat& src, cv::Mat& dst) const{
    dst = src.clone();
    dst /= 255;         // 0は0 , 1以上は1に変換される

    cv::Mat prev = cv::Mat::zeros(dst.size(), CV_8U);
    cv::Mat diff;

    do {
        thinningIteration_uhar(dst, 0);
        thinningIteration_uhar(dst, 1);
        cv::absdiff(dst, prev, diff);
        dst.copyTo(prev);
    } while (countNonZero(diff) > 0);

    dst *= 255;
}

void My_defined_space::thinning::thinningIteration_float(const cv::Mat& img, int iter) const{
    cv::Mat del_marker = cv::Mat::ones(img.size(), CV_32FC1);
    int x, y;

    for (y = 1; y < img.rows - 1; ++y) {

        for (x = 1; x < img.cols - 1; ++x) {

            int v9, v2, v3;
            int v8, v1, v4;
            int v7, v6, v5;

            v1 = img.data[y   * img.step + x   * img.elemSize()];
            v2 = img.data[(y - 1) * img.step + x   * img.elemSize()];
            v3 = img.data[(y - 1) * img.step + (x + 1) * img.elemSize()];
            v4 = img.data[y   * img.step + (x + 1) * img.elemSize()];
            v5 = img.data[(y + 1) * img.step + (x + 1) * img.elemSize()];
            v6 = img.data[(y + 1) * img.step + x   * img.elemSize()];
            v7 = img.data[(y + 1) * img.step + (x - 1) * img.elemSize()];
            v8 = img.data[y   * img.step + (x - 1) * img.elemSize()];
            v9 = img.data[(y - 1) * img.step + (x - 1) * img.elemSize()];

            int S = (v2 == 0 && v3 == 1) + (v3 == 0 && v4 == 1) +
                (v4 == 0 && v5 == 1) + (v5 == 0 && v6 == 1) +
                (v6 == 0 && v7 == 1) + (v7 == 0 && v8 == 1) +
                (v8 == 0 && v9 == 1) + (v9 == 0 && v2 == 1);

            int N = v2 + v3 + v4 + v5 + v6 + v7 + v8 + v9;

            int m1 = 0, m2 = 0;

            if (iter == 0) m1 = (v2 * v4 * v6);
            if (iter == 1) m1 = (v2 * v4 * v8);

            if (iter == 0) m2 = (v4 * v6 * v8);
            if (iter == 1) m2 = (v2 * v6 * v8);

            if (S == 1 && (N >= 2 && N <= 6) && m1 == 0 && m2 == 0)
                del_marker.data[y * del_marker.step + x * del_marker.elemSize()] = 0;
        }
    }

    img &= del_marker;
}
void  My_defined_space::thinning::thinning_float(const cv::Mat& src, cv::Mat& dst) const {
    dst = src.clone();
    cv::Mat prev = cv::Mat::zeros(dst.size(), CV_32FC1);
    cv::Mat diff;

    do {
        thinningIteration_float(dst, 0);
        thinningIteration_float(dst, 1);
        cv::absdiff(dst, prev, diff);
        dst.copyTo(prev);
    } while (countNonZero(diff) > 0);
}



void My_defined_space::Plot_Histgram::
    Plot_Histgram_func
    (const std::string hist_wiidow_name, const cv::Mat &src,  int histSize_nn,  float rang_max, float rang_min) const {
    float range[] = { rang_min, rang_max };
    const float* histRange = { range };
    std::cout << "&range = " << &range << std::endl;
    std::cout << "histRange = " << histRange << std::endl;
    std::cout << "&histRange = " << &histRange << std::endl;
    cv::Mat histImg;
    cv::calcHist(&src, 1, 0, cv::Mat(), histImg, 1, &histSize_nn, &histRange);
    //cv::imshow("hist" + hist_wiidow_name, histImg);
    cv::Mat showHistImg(histSize_nn, histSize_nn, CV_8U, cv::Scalar(rang_max));  //把直方圖秀在一個256*256大的影像上
    drawHistImg(histImg, showHistImg, histSize_nn, rang_max, rang_min);
    cv::imshow(hist_wiidow_name, showHistImg);
}

void My_defined_space::Plot_Histgram::
    drawHistImg(const cv::Mat &src, cv::Mat &dst,  int histSize_nn,  float rang_max,  float rang_min) const
{
    float histMaxValue = 0;
    for (int i = 0; i<histSize_nn; i++) {
        float tempValue = src.at<float>(i);
        if (histMaxValue < tempValue) {
            histMaxValue = tempValue;
        }
    }

    float scale = ((float)0.9 * (float)histSize_nn) / histMaxValue;
    for (int i = 0; i<histSize_nn; i++) {
        int intensity = static_cast<int>(src.at<float>(i)*scale);
        line(dst, cv::Point(i, rang_max), cv::Point(i, rang_max - intensity), cv::Scalar(0));
    }
}


void My_defined_space::Find_Chara_FingerPrint::Find_Chara_FingerPrint_uchar
(const cv::Mat & image_input, cv::Mat& image_output, std::vector<cv::KeyPoint>& keypoints,
    int Harris_blockSize, int Harris_aperture_Size, double k) const
{

    cv::Mat harris_corners, harris_normalised;
    cornerHarris(image_input, harris_corners, Harris_blockSize, Harris_aperture_Size, k, cv::BORDER_DEFAULT);
    normalize(harris_corners, harris_normalised, 0, 255, cv::NORM_MINMAX, CV_32FC1);

    float threshold = 125.0;

    cv::Mat rescaled;
    convertScaleAbs(harris_normalised, rescaled);
    cv::Mat harris_c(rescaled.rows, rescaled.cols, CV_8UC3);
    cv::Mat in[] = { rescaled, rescaled, rescaled };
    int from_to[] = { 0,0, 1,1, 2,2 };
    cv::mixChannels(in, 3, &harris_c, 1, from_to, 3);
    for (int x = 0; x<harris_normalised.cols; x++) {
        for (int y = 0; y<harris_normalised.rows; y++) {
            if (harris_normalised.at<float>(y, x) > threshold) {
                // Draw or store the keypoint location here, just like you decide. In our case we will store the location of the keypoint
                circle(harris_c, cv::Point(x, y), 5, cv::Scalar(0, 255, 0), 1);
                circle(harris_c, cv::Point(x, y), 1, cv::Scalar(0, 0, 255), 1);
                keypoints.push_back(cv::KeyPoint(x,y, 1));
            }
        }
    }
    image_output = harris_c.clone();
}

void My_defined_space::Find_Chara_FingerPrint::Find_Chara_FingerPrint_float
(const cv::Mat & image_input, cv::Mat& image_output, std::vector<cv::KeyPoint>& keypoints,
    int Harris_blockSize, int Harris_aperture_Size, double k) const
{
    cv::Mat harris_corners, harris_normalised;
    cornerHarris(image_input, harris_corners, Harris_blockSize, Harris_aperture_Size, k, cv::BORDER_DEFAULT);
    normalize(harris_corners, harris_normalised, 0, 255, cv::NORM_MINMAX, CV_32FC1);

    float threshold = 125;

    cv::Mat rescaled;
    convertScaleAbs(harris_normalised, rescaled);
    cv::Mat harris_c(rescaled.rows, rescaled.cols, CV_8UC3);
    cv::Mat in[] = { rescaled, rescaled, rescaled };
    int from_to[] = { 0,0, 1,1, 2,2 };
    cv::mixChannels(in, 3, &harris_c, 1, from_to, 3);
    for (int x = 0; x<harris_normalised.cols; x++) {
        for (int y = 0; y<harris_normalised.rows; y++) {
            if (harris_normalised.at<float>(y, x) > threshold) {
                // Draw or store the keypoint location here, just like you decide. In our case we will store the location of the keypoint
                circle(harris_c, cv::Point(x, y), 5, cv::Scalar(0, 255, 0), 1);
                circle(harris_c, cv::Point(x, y), 1, cv::Scalar(0, 0, 255), 1);
                keypoints.push_back(cv::KeyPoint(x, y, 1));
            }
        }
    }

    image_output = harris_c.clone();
}
void My_defined_space::Inverse_gray_value::
    Inverse_gray_value_uchar
    (const cv::Mat & src, cv::Mat & dst) const{
    //dst = cv::Scalar::all(255) - src;
    cv::Mat dst_buf(src.rows, src.cols, CV_8U);
    for (int i = 0; i < src.rows ; i++)
    {
        for (int j = 0; j < src.cols; j++)
        {
                dst_buf.at<uchar>(i,j) =
                    static_cast<uchar>(255) - src.at<uchar>(i, j);
        }
    }
    dst = dst_buf.clone();
}

void  My_defined_space::gabor_filter::
        gabor_filter_func
(const cv::Mat& src_f, cv::Mat& dest,int kernel_size, double sig, double th, double lm, double gm, double ps) const
{
    cv::Mat src_buf , dest_buf;

    src_f.convertTo(src_buf, CV_32F);
    cv::Mat kernel = cv::getGaborKernel(cv::Size(kernel_size, kernel_size), sig, th, lm, gm, ps, CV_32F);
    cv::filter2D(src_buf, dest_buf, CV_32F, kernel);
    dest = dest_buf.clone();
    //cv::filter2D(src_buf, dest_buf, CV_32F, kernel);
    //dest_buf.convertTo(dest, CV_8U, 1.0 / 255.0);
    //cv::imshow("k", kernel);
    //cv::imshow("in_gabor", dest_buf);
}

void My_defined_space::Otsu_float::Otsu_threshold_float(const cv::Mat In_image, cv::Mat Out_image)
{
    cv::Mat Out_buf;
    float Otsu_threshold = getOtsuThreshold(In_image);
    float mean_fore, weight_fore, variance_fore;
    float mean_bg, weight_bg, variance_bg;

    cv::Mat mean_fore_mat, mean_bg_mat;
    cv::Mat mean_mat(In_image.rows , In_image.cols , CV_32FC1), stddev_mat(In_image.rows, In_image.cols, CV_32FC1);
    cv::meanStdDev(In_image, mean_mat, stddev_mat);

    float mean = mean_mat.at<float>(0,0);
    float stddev = stddev_mat.at<float>(0, 0);
    std::cout << "mean = " << mean << "std = " << stddev << std::endl;

    for (int i = 0 ; i < In_image.rows;i++)
        for (int j = 0; j < In_image.cols; j++)
        {
            if (In_image.at<float>(i, j) > Otsu_threshold)
                Out_buf.at<float>(i, j) = 1;
            else
                Out_buf.at<float>(i, j) = 0;
        }
}

float My_defined_space::Otsu_float::getOtsuThreshold(const cv::Mat hist)
{
    float Otsu_threshold;


    std::cout << "float_Otsu_threshold = "<< Otsu_threshold << std::endl;
    return Otsu_threshold;
}
