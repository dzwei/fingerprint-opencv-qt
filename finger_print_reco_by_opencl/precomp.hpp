﻿#ifndef PRECOMP_HPP
#define PRECOMP_HPP
    #include <QCoreApplication>
    #include <QFile>
    //used of c header
    #include <stdio.h>
    #include <math.h>
    #define _USE_MATH_DEFINES // for C++
    #include <cmath>
    //used of  /c++ header
    #include <sstream>
    #include <iostream>
    #include <vector>
    #include <array>
    //used of opencv header
    #include "opencv2/core/core.hpp"
    #include "opencv2/highgui/highgui.hpp"
    #include "opencv2/imgproc/imgproc.hpp"
    #include "opencv2/features2d/features2d.hpp"
    //use of opencl header
    #ifdef __APPLE__
    #include <OpenCL/opencl.h> //if in apple platform
    #else
    #include <CL/cl.h> //if in windows platform
    #endif
    //our libs
    //#include "My_Defined.hpp"

#endif // PRECOMP_HPP
