﻿#ifndef MY_DEFINED_HPP
#define MY_DEFINED_HPP
//Declear myself function in this header
/*
Myself defined
*/
namespace My_defined_space {

class Find_Chara_FingerPrint
{
public:
    void Find_Chara_FingerPrint_uchar
    (const cv::Mat & image_input, cv::Mat & image_output, std::vector<cv::KeyPoint>& keypoints,
        int Harris_blockSize,int Harris_aperture_Size,double k) const;

    void Find_Chara_FingerPrint_float
    (const cv::Mat & image_input, cv::Mat& image_output, std::vector<cv::KeyPoint>& keypoints,
        int Harris_blockSize, int Harris_aperture_Size, double k) const;
private:

};

class gabor_filter
{
public:
    void gabor_filter_func
    (const cv::Mat& src_f, cv::Mat& dest,int kernel_size, double sig,double th ,double lm ,double gm,double ps) const;
private:

};


class thinning
{
public:
    void thinning_uhar(const cv::Mat& src, cv::Mat& dst) const ;
    void thinning_float(const cv::Mat& src, cv::Mat& dst) const;
private:
    void thinningIteration_uhar(const cv::Mat& im, int iter) const;
    void thinningIteration_float(const cv::Mat& im, int iter) const;
};

class Plot_Histgram
{
public:
    void Plot_Histgram_func
        (const std::string hist_wiidow_name, const cv::Mat &src, int histSize_nn, float rang_max, float rang_min) const;
        //(const std::string hist_wiidow_name, const cv::Mat &src, cv::Mat& output_hist, const int histSize_nn, const float rang_max, const float rang_min) const;
private:
    void drawHistImg(const cv::Mat &src, cv::Mat &dstconst , int histSize_nn,  float rang_max, float rang_min) const;
};

class Disp_img_And_Show_Out_type
{
public:
    //Display image and show type of output image
    //Disp_img_And_Show_Out_type(const string Your_define_name_of_input_image , const cv::Mat& input_image)
    void Disp_img_And_Show_Out_type_func
        (const std::string input_str, const cv::Mat& input_image) const;
private:

        //return type of input_image
        //how to use? => getImageType(myImage.type()) type of myImage is Mat
        std::string getImageType(int number) const;
        //-----------------------------------------

};

class polluting
{
public:
    /*polluted input array with random noise(gauss noise)
    //==output type is uchar==//
    argument(float polluted_rate , const cv::Mat& input_array, cv::Mat& output_array)*/
    //header don't declear default argument
    void polluting_uchar
        (double polluted_average, double polluted_std_dev, const cv::Mat& input_array, cv::Mat& output_array) const;
    /*polluted input array with random noise(gauss noise)
    argument(float polluted_rate , const cv::Mat& input_array, cv::Mat& output_array)*/
    void polluting_double
        (double polluted_average, double polluted_std_dev, const cv::Mat& input_array, cv::Mat& output_array) const;
    //header don't declear default argument
private:

};

	
class Inverse_gray_value
{
public:
    void Inverse_gray_value_uchar(const cv::Mat & src, cv::Mat & dst) const;
private:

};

class Otsu_float
{
public:
    void Otsu_threshold_float(const cv::Mat In_image, cv::Mat Out_image);
private:
    float getOtsuThreshold(const cv::Mat hist);
};




//======= inline defined=======
inline void My_defined_space::Disp_img_And_Show_Out_type::
    Disp_img_And_Show_Out_type_func
    (const std::string input_str, const cv::Mat& input_image) const{
    //inline func need declear and implement in .h file
    cv::namedWindow(input_str, cv::WINDOW_AUTOSIZE);//establish image window and set size_adjest of display image in window
    cv::imshow(input_str, input_image);//load image to window
    std::cout << "type of " << input_str << " : " << My_defined_space::Disp_img_And_Show_Out_type::getImageType(input_image.type()) << std::endl;
}
inline std::string  My_defined_space::Disp_img_And_Show_Out_type::getImageType(int number) const
{
    // find type
    int imgTypeInt = number % 8;
    std::string imgTypeString;

    switch (imgTypeInt)
    {
    case 0:
        imgTypeString = "8U";
        break;
    case 1:
        imgTypeString = "8S";
        break;
    case 2:
        imgTypeString = "16U";
        break;
    case 3:
        imgTypeString = "16S";
        break;
    case 4:
        imgTypeString = "32S";
        break;
    case 5:
        imgTypeString = "32F";
        break;
    case 6:
        imgTypeString = "64F";
        break;
    default:
        break;
    }
    // find channel
    int channel = (number / 8) + 1;

    std::stringstream type;
    type << "CV_" << imgTypeString << "C" << channel;
    return type.str();
}


}

#endif



