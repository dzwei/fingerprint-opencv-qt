﻿#include <QCoreApplication>
#include <QDir>
#include <QDebug>
//used of c header
#include <stdio.h>
#include <math.h>
#define _USE_MATH_DEFINES // for C++
#include <cmath>
//used of  /c++ header
#include <sstream>
#include <iostream>
#include <vector>
#include <array>
//used of opencv header
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d/features2d.hpp"
//use of opencl header
#ifdef __APPLE__
#include <OpenCL/opencl.h> //if in apple platform
#else
#include <CL/cl.h> //if in windows platform
#endif

#include "My_Defined.hpp"

using namespace std; //std::string
using namespace cv; //cv::String
using namespace My_defined_space;

int main(int argc , char* argv[4])// only read one image  ,argv[0] is file name ,
                                                     //argv[1] is path of input image
                                                     //argv[2] is polluted_stddev of noise ,
                                                     //argv[3] is polluted_average of noise
{
    QCoreApplication a(argc, argv);
    My_defined_space::gabor_filter gabor_filter;
    My_defined_space::thinning thining;
    My_defined_space::Find_Chara_FingerPrint Find_Chara_FingerPrint;
    My_defined_space::Disp_img_And_Show_Out_type Disp_img_And_Show_Out_type;
    My_defined_space::polluting polluting;
    My_defined_space::Plot_Histgram Plot_Histgram;
    My_defined_space::Inverse_gray_value Inverse_gray_value;

//===========================================================================================



        const string path_of_loaded_image = "../fp_lib/well_fp_1.jpg";
        //const string path_of_loaded_image = "C:\\Users\\finis\\Desktop\\finger_print_reco_by_opencl\\build-finger_print_reco_by_opencl-Desktop_Qt_5_9_1_MSVC2017_64bit-Debug\\debug\\fp_lib\\well_fp_1.jpg";
        //const string path_of_loaded_image = "C:\\fp_lib\\well_fp_3.jpg";
        struct Enable {
            bool uchar_polluet = false; //indicate that degree of noise
            bool Thining_en = true;
            bool Show_Hist_Otsu_comp = true ;
        };
        Enable Enable;


        struct in_the_uchar_mode {
            double pollute_avg = 128;
            double polluted_stddev = 128;
        };
        in_the_uchar_mode in_the_uchar_mode;

        struct in_the_double_mode {
            double  pollute_avg = 0.0;
            double  polluted_stddev = 0.2;
        };
        in_the_double_mode in_the_double_mode;


        enum filter_type{
            No = 0, Gauss_filter = 1, Gabor_filter = 2
        };
        filter_type filter_type = Gauss_filter;

        struct Parameter_GaussianBlur {
            int GaussianBlur_size_nn = 7;
            double GaussianBlur_stddev_x = 1;
            double GaussianBlur_stddev_y = 1;
        };
        Parameter_GaussianBlur Parameter_GaussianBlur;

        struct Parameter_Garbor_filter {
            int kernel_size = 32;
            double sig = 1.0;
            double th = 45;
            double lm = 1.1;
            double gm = 0.02;
            double ps = 0;
        //Parameters:
        //	ksize  Size of the filter returned.
        //		sigma  Standard deviation of the gaussian envelope.
        //		theta  Orientation of the normal to the parallel stripes of a Gabor function.
        //		lambd  Wavelength of the sinusoidal factor.
        //		gamma  Spatial aspect ratio.
        //		psi  Phase offset.
        //		ktype  Type of filter coefficients.It can be CV_32F or CV_64F .
        };
        Parameter_Garbor_filter Parameter_Garbor_filter;

        struct Parameter_Harris_corner_Detector {
            int block_nn = 2;
            int aperture_size_nn = 3;
            double k = 0.04;
        };
        Parameter_Harris_corner_Detector  Parameter_Harris_corner_Detector;

       struct Parameter_Hist{
           int size_nn = 300;
           float val_rang_max = 255;
           float val_rang_min = 0;
       };
       Parameter_Hist Parameter_Hist;



//===========================================================================================

       cout << "Basical parameter :  " << endl;
       cout << "------------------------------------------" << endl;
              cout << "Show_Hist_Otsu_comp = " << Enable.Show_Hist_Otsu_comp << "\n"
                   << "Thining_en = " << Enable.Thining_en << endl;
               cout << "Type of filter = ";
               switch (filter_type)
               {
               default: cout << "No" << endl;
                   break;
               case Gauss_filter:
                   cout << "Gauss_filter" << endl;
                   break;
               case Gabor_filter :
                   cout << "Gabor_filter" << endl;
                   break;
               }

               if (Enable.uchar_polluet == true) {
                   cout << "uchar_polluet" << endl;
               }
               else {
                   cout << "double_polluet" << endl;
               }


               cout << "=============================================" << endl;


    Mat original_image_in = imread(path_of_loaded_image, CV_LOAD_IMAGE_GRAYSCALE); //input image for image by program defaulted
    Mat original_image_buf;//decided input image by bash_parament_in or program defaults
    if (argc > 1) { //cheak if use bash_parament_in
        const std::string bash_parament_in(argv[1]);
        original_image_in = imread(bash_parament_in, CV_LOAD_IMAGE_GRAYSCALE);
        cout << "read-in image by bash_parament : " << bash_parament_in << endl;
        original_image_in.copyTo(original_image_buf);
        if (argc > 2) {
            if (Enable.uchar_polluet == true) {
                in_the_uchar_mode.polluted_stddev = strtod(argv[2], NULL);
                cout << "pollute_avg_uchar = " << in_the_uchar_mode.polluted_stddev << endl;
            }
            else
            {
                in_the_double_mode.polluted_stddev = strtod(argv[2], NULL);
                cout << "pollute_avg = " << in_the_double_mode.polluted_stddev << endl;
            }
        }
        if (argc > 3) {
            if (Enable.uchar_polluet == true) {
                in_the_uchar_mode.pollute_avg = strtod(argv[3], NULL);
                cout << "pollute_avg_uchar = " << in_the_uchar_mode.pollute_avg << endl;
            }
            else
            {
                in_the_double_mode.pollute_avg = strtod(argv[3], NULL);
                cout << "pollute_avg = " << in_the_double_mode.pollute_avg << endl;
            }
        }
    }
    else {

        cout << "can't find path of bash_parament \n=> read-in image by program defaulte : "<< path_of_loaded_image << endl;
        original_image_in.copyTo(original_image_buf);
        if (Enable.uchar_polluet == true)
            cout << "default of polluted_stddev_uchar = " << in_the_uchar_mode.polluted_stddev << " ; default of pollute_avg_uchar = " << in_the_uchar_mode.pollute_avg << endl;
        else
            cout << "default of polluted_stddev_double = " << in_the_double_mode.polluted_stddev << " ; default of pollute_avg_double = " << in_the_double_mode.pollute_avg << endl;
    }
    const Mat original_image_noninv = original_image_buf.clone(); //can't put in jedgment above loading-jedgment

        try
        {
            if (!original_image_noninv.data)  throw
        "can't find path of loaded image\nNeither bash_parament nor program defaulte,\nplease cheak image of loadding_paht : ";
        }
        catch (const char* exceptional_message)
        {
            //can't find path of loaded image,please cheak image of loadding_paht
            cout << exceptional_message << "\t";
        }

        Mat original_image_inv;
        Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("original_image_noninv", original_image_noninv);
        Inverse_gray_value.Inverse_gray_value_uchar(original_image_noninv, original_image_inv);
        const Mat original_image = original_image_inv.clone();
        Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("original_image_inv", original_image);



Mat polluted_image;
if (Enable.uchar_polluet == true) {
    polluting.polluting_uchar(in_the_uchar_mode.pollute_avg, in_the_uchar_mode.polluted_stddev, original_image, polluted_image);
}
else {
    polluting.polluting_double(in_the_double_mode.pollute_avg, in_the_double_mode.polluted_stddev, original_image, polluted_image);
}
Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("polluted_image", polluted_image);




    Mat Handel_in;
    //if (Enable.uchar_polluet == true || filter_type == Gabor_filter) {
    if (Enable.uchar_polluet == true){
        Handel_in = polluted_image.clone();
    }
    else{
        normalize(polluted_image, Handel_in, 0, 255, CV_MINMAX, CV_8U);//img_char to img_float
        Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Handel_in", Handel_in);
    }

            Mat Handel_in_thining;
            Mat Chara_Handel_in_thining;
            std::vector<cv::KeyPoint> Keypoints_Handel_in;
            if (Enable.Thining_en == true) { // only probe thinihn image and information , can't output to next satge
                Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Handel_in", Handel_in);
                thining.thinning_uhar(Handel_in, Handel_in_thining);
                Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Handel_in_thining", Handel_in_thining);
            }
            else {
                Handel_in_thining = Handel_in.clone();
            }
            Find_Chara_FingerPrint.Find_Chara_FingerPrint_uchar
            (Handel_in_thining, Chara_Handel_in_thining, Keypoints_Handel_in,
                Parameter_Harris_corner_Detector.block_nn,
                Parameter_Harris_corner_Detector.aperture_size_nn,
                Parameter_Harris_corner_Detector.k);
            Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Chara_Handel_in_thining (polluted)", Chara_Handel_in_thining);

            Mat Otsu_image;

            if (Enable.Show_Hist_Otsu_comp == true)
                Plot_Histgram.Plot_Histgram_func
                ("Handel_in_Hist", Handel_in, Parameter_Hist.size_nn, Parameter_Hist.val_rang_max, Parameter_Hist.val_rang_min);

            cv::threshold(Handel_in, Otsu_image, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);

            if (Enable.Show_Hist_Otsu_comp == true)
                Plot_Histgram.Plot_Histgram_func
                ("Otsu_image_Hist", Otsu_image, Parameter_Hist.size_nn, Parameter_Hist.val_rang_max, Parameter_Hist.val_rang_min);


            Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Otsu_image", Otsu_image);

            Mat fliter_image;

            if (filter_type == Gauss_filter) {
                GaussianBlur(Otsu_image, fliter_image,
                    Size(Parameter_GaussianBlur.GaussianBlur_size_nn,
                        Parameter_GaussianBlur.GaussianBlur_size_nn),
                    Parameter_GaussianBlur.GaussianBlur_stddev_x,
                    Parameter_GaussianBlur.GaussianBlur_stddev_y);
            }
            else if (filter_type == Gabor_filter)
            {
                gabor_filter.gabor_filter_func
                (Otsu_image, fliter_image,
                    Parameter_Garbor_filter.kernel_size,
                    Parameter_Garbor_filter.sig, Parameter_Garbor_filter.th,
                    Parameter_Garbor_filter.lm, Parameter_Garbor_filter.gm,
                    Parameter_Garbor_filter.ps
                );
            }
            else {
                fliter_image = Otsu_image.clone();
            }
            Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("after_fliter_image", fliter_image);

    Mat Chara_polluted_recover_image , after_filter_to_thining_imag;
    std::vector<cv::KeyPoint> Keypoints_polluted_image;
    if (Enable.Thining_en == true) {
        if (filter_type == Gabor_filter)
        {
            thining.thinning_float(fliter_image, after_filter_to_thining_imag);
            Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("after_thining_of_filter_image", after_filter_to_thining_imag);
            Find_Chara_FingerPrint.Find_Chara_FingerPrint_float(after_filter_to_thining_imag, Chara_polluted_recover_image, Keypoints_polluted_image,
            Parameter_Harris_corner_Detector.block_nn,
            Parameter_Harris_corner_Detector.aperture_size_nn,
            Parameter_Harris_corner_Detector.k);
        }
        else
        {
            thining.thinning_uhar(fliter_image, after_filter_to_thining_imag);
            Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("after_thining_of_filter_image", after_filter_to_thining_imag);
            Find_Chara_FingerPrint.Find_Chara_FingerPrint_uchar(after_filter_to_thining_imag, Chara_polluted_recover_image, Keypoints_polluted_image,
                Parameter_Harris_corner_Detector.block_nn,
                Parameter_Harris_corner_Detector.aperture_size_nn,
                Parameter_Harris_corner_Detector.k);
        }
    }
    else {
            after_filter_to_thining_imag = fliter_image.clone();
            if (filter_type == Gabor_filter)
                Find_Chara_FingerPrint.Find_Chara_FingerPrint_uchar(after_filter_to_thining_imag, Chara_polluted_recover_image, Keypoints_polluted_image,
                    Parameter_Harris_corner_Detector.block_nn,
                    Parameter_Harris_corner_Detector.aperture_size_nn,
                    Parameter_Harris_corner_Detector.k);
            else
                Find_Chara_FingerPrint.Find_Chara_FingerPrint_float(after_filter_to_thining_imag, Chara_polluted_recover_image, Keypoints_polluted_image,
                    Parameter_Harris_corner_Detector.block_nn,
                    Parameter_Harris_corner_Detector.aperture_size_nn,
                    Parameter_Harris_corner_Detector.k);
    }
    Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Chara_polluted_recover_image", Chara_polluted_recover_image);

    Mat Chara_Original_image, original_image_thining;
    std::vector<cv::KeyPoint> Keypoints_Original_image;

    if (Enable.Thining_en == true) {
        thining.thinning_uhar(original_image, original_image_thining);
        Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("original_image_thining", original_image_thining);
    }
    else{
        original_image_thining = original_image.clone();
    }
    Find_Chara_FingerPrint.Find_Chara_FingerPrint_uchar
    (original_image_thining, Chara_Original_image,Keypoints_Original_image,
        Parameter_Harris_corner_Detector.block_nn,
        Parameter_Harris_corner_Detector.aperture_size_nn,
        Parameter_Harris_corner_Detector.k);
    Disp_img_And_Show_Out_type.Disp_img_And_Show_Out_type_func("Chara_Original_image", Chara_Original_image);




//---------------------------------------------------------------------
    cv::waitKey(0);
    //system("pause"); // need use waitkey
    return 0;
//---------------------------------------------------------------------



}
