QT -= gui

CONFIG += c++17 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    My_Defined.cpp

HEADERS += \
    precomp.hpp \
    My_Defined.hpp

PRECOMPILED_HEADER += precomp.hpp


#opencv path : you need to check it vailed frist
windows {
        message("===============================================")
        message(">>>> on the win32 or win32 64bits platform <<<<")
        message("===============================================")

    #check -1 : Is OPENCV_ROOT_PATH correct?
    OPENCV_ROOT_PATH = "C:/opencv/build"
    message("OPENCV_ROOT_PATH : $${OPENCV_ROOT_PATH}")

    #check -2 : Is OPENCV_INCLUDE_ROOT_PATH correct?
    OPENCV_INCLUDE_ROOT_PATH = "$${OPENCV_ROOT_PATH}/include"
    message("OPENCV_INCLUDE_ROOT_PATH : $${OPENCV_INCLUDE_ROOT_PATH}")

    #check -3 : Is OPENCV_LIBS_ROOT_PATH correct?
    OPENCV_LIBS_ROOT_PATH = "C:/opencv/build/x64/vc15/lib"
    message("OPENCV_LIBS_ROOT_PATH : $${OPENCV_LIBS_ROOT_PATH}")

    INCLUDEPATH += $${OPENCV_INCLUDE_ROOT_PATH}

    #check -4 :used lib that it's your need?
    CONFIG(debug,debug|release) {
        LIBS += $${OPENCV_LIBS_ROOT_PATH}/opencv_world341d.lib
    } else {
        LIBS += $${OPENCV_LIBS_ROOT_PATH}/opencv_world341.lib
    }
}
linux {
        message("===============================")
        message(">>>> on the linux platform <<<<")
        message("===============================")

    #check -1 : Is OPENCV_ROOT_PATH correct?
    OPENCV_ROOT_PATH = "/usr/local"
    message("OPENCV_ROOT_PATH : $${OPENCV_ROOT_PATH}")

    #check -2 : Is OPENCV_INCLUDE_ROOT_PATH correct?
    OPENCV_INCLUDE_ROOT_PATH = "$${OPENCV_ROOT_PATH}/include"
    message("OPENCV_INCLUDE_ROOT_PATH : $${OPENCV_INCLUDE_ROOT_PATH}")

    #check -3 : Is OPENCV_LIBS_ROOT_PATH correct?
    OPENCV_LIBS_ROOT_PATH = "$${OPENCV_ROOT_PATH}/lib"
    message("OPENCV_LIBS_ROOT_PATH : $${OPENCV_LIBS_ROOT_PATH}")

    #check -4 :used lib need to prefix "-l" and it's correct and it's your need?
    OPENCV_LIBS_USED += -lopencv_core
    OPENCV_LIBS_USED += -lopencv_imgcodecs
    OPENCV_LIBS_USED += -lopencv_highgui
    OPENCV_LIBS_USED += -lopencv_imgproc
    message("OPENCV_LIBS_USED : $${OPENCV_LIBS_USED}")

    INCLUDEPATH += $${OPENCV_INCLUDE_ROOT_PATH}
    LIBS += -L$${OPENCV_LIBS_ROOT_PATH} $${OPENCV_LIBS_USED}

}
